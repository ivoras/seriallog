#!/usr/bin/python3
import sys,os,re
from datetime import datetime

RE_STRIP = re.compile(r'(.*)[ \n\r\t]*$')

def strip(ln):
    m = RE_STRIP.match(ln)
    return m.group(1)

def dump(ln):
    for ch in ln:
        sys.stdout.write(str(ord(ch)))
        sys.stdout.write(' ')
    sys.stdout.write('\n')

s = open('/dev/ttyUSB0', 'r')
log = open('serial.log', 'a+')

for line in s:
    line = strip(line)
    if line == '': continue
    #dump(line)
    now = datetime.today().replace(microsecond=0)
    line = now.isoformat() + ' ' + line + '\n'
    log.write(line)
    sys.stdout.write(line)
    
